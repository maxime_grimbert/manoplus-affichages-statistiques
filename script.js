// Constructeurs

// Functions par sections

// Import export

// Import MANO

let xlsxImportButton = document.querySelector('#xlsx_import_button');
xlsxImportButton.onclick = xlsxImport;

let manoDataParsed;

function xlsxImport() {
    let xlsxImportFileSelector = document.querySelector('#xlsx_import_file_selector');
    let manoDataFile = xlsxImportFileSelector.files[0]; // Select the first file gave to the "parcourir" input.

    // Debug
    console.log("Debug : manoDataFile");
    console.log(manoDataFile);

    let fileReader = new FileReader(); // This is a geenric tool.

    fileReader.readAsArrayBuffer(manoDataFile); // Actually reads the file and will automaticly trigger onload when done reading (back-office)

    // Debug
    console.log("fileReader started to read manoDataFile as ArrayBuffer");

    fileReader.onload = function() { // This tells the tool that when it will later and async read successfully a file, it will have to do the following function.
        let manoDataRead = fileReader.result;
        // Debug
        console.log("Debug : manoDataRead");
        console.log(manoDataRead);

        manoDataParsed = XLSX.read(manoDataRead);
        // Debug
        console.log("Debug : manoDataParsed");
        console.log(manoDataParsed);


    };

    
}


// Affichages statistiques

let statsDisplayButton = document.querySelector('#stats_display_button');
statsDisplayButton.onclick = updateStatsDisplay;

let statsDisplayDiv = document.querySelector('#updatedStatsDisplay');

function updateStatsDisplay() {
    // Cette fonction est le coeur du dispositif. Idéalement ensuite elle sera adapté en fonction des modules stats importés en json.
    // Debug
    updateTestStatsDisplay(); // Cette ligne pour debug et tests uniquement. On peut la comment out ensuite lorsque les utilisateur.rice.s pourront effectivement importer leurs modules stats.
}

function updateTestStatsDisplay() {
    
    // Extraction

    let tousLesTerritoires = [];

    let arrayDesEmplacementsDesCellulesDeLaFeuilleTerritoires = Object.keys(manoDataParsed.Sheets["territoires"]);

    console.log("Debug : array des emplacements de Territoires");
    console.log(arrayDesEmplacementsDesCellulesDeLaFeuilleTerritoires);

    arrayDesEmplacementsDesCellulesDeLaFeuilleTerritoires.forEach(emplacementDeCelluleDeLaFeuilleTerritoires => {

        if (emplacementDeCelluleDeLaFeuilleTerritoires.split('')[0] == "A" && emplacementDeCelluleDeLaFeuilleTerritoires != "A1")
        {
            tousLesTerritoires.push({"id":manoDataParsed.Sheets["territoires"][emplacementDeCelluleDeLaFeuilleTerritoires].v});
        }
    });

    tousLesTerritoires.forEach(territoire => {
        arrayDesEmplacementsDesCellulesDeLaFeuilleTerritoires.forEach(emplacementDeCelluleDeLaFeuilleTerritoires => {

            if (manoDataParsed.Sheets["territoires"][emplacementDeCelluleDeLaFeuilleTerritoires].v == territoire["id"])
            {
                let numeroDeLigne = emplacementDeCelluleDeLaFeuilleTerritoires.split('')[1];

                arrayDesEmplacementsDesCellulesDeLaFeuilleTerritoires.forEach(emplacementDeCelluleDeLaFeuilleTerritoires2 => {

                    if (emplacementDeCelluleDeLaFeuilleTerritoires2 == ("E" + numeroDeLigne))
                    {    
                        territoire["name"] = manoDataParsed.Sheets["territoires"][emplacementDeCelluleDeLaFeuilleTerritoires2].v;
                    }
                });

            }
        });
    });

    let arrayDesEmplacementsDesCellulesDeLaFeuilleObservations = Object.keys(manoDataParsed.Sheets["observations de territoires"]);

    tousLesTerritoires.forEach(territoire => {
        arrayDesEmplacementsDesCellulesDeLaFeuilleObservations.forEach(emplacementDeCelluleDeLaFeuilleObservations => {
            if (manoDataParsed.Sheets["observations de territoires"][emplacementDeCelluleDeLaFeuilleObservations].v == territoire["id"])
            {
                console.log("ca match");
                if (territoire["occurrences"] != undefined && territoire["occurrences"] != null && territoire["occurrences"] != 0)
                {
                    console.log(territoire["occurrences"]);

                    console.log("+ 1");

                    territoire["occurrences"] += 1;

                    console.log(territoire["occurrences"]);
                }
                else {
                    console.log(territoire["occurrences"]);
                    console.log("remise à 1");

                    territoire["occurrences"] = 1;

                    console.log(territoire["occurrences"]);
                }
            }
        });
    });


    console.log("Debug : ");
    console.log(tousLesTerritoires);

    // Affichage

    // Vider la div
    statsDisplayDiv.innerHTML = '';

    // Créer le tableau et le mettre dans la div   
    let maraudesParTerritoiresTable = document.createElement('table');
    statsDisplayDiv.appendChild(maraudesParTerritoiresTable);

    // Créer le header et le mettre dans le tableau
    let header = document.createElement('thead');
    maraudesParTerritoiresTable.appendChild(header);
    let rowHead = document.createElement('tr');
    header.appendChild(rowHead);
    let territoiresHead = document.createElement('th');
    territoiresHead.textContent = "Territoire";
    rowHead.appendChild(territoiresHead);
    let maraudesHead = document.createElement('th');
    maraudesHead.textContent = "Nombre de maraudes effectuées sur ce territoire";
    rowHead.appendChild(maraudesHead);

    // Créer le body et le mettre dans le tableau
    let body = document.createElement('tbody');
    maraudesParTerritoiresTable.appendChild(body);

    // Créer les row et les cellules avec une boucle et les mettre dans le body

    tousLesTerritoires.forEach(territoire => {
        // Créer une ligne et l'ajouter au tableau
        let rowBody = document.createElement('tr');
        body.appendChild(rowBody);

        // Remplir la case "territoire" de cette ligne
        let territoiresNameCell = document.createElement('td');
        territoiresNameCell.textContent = territoire["name"];
        rowBody.appendChild(territoiresNameCell);

        // Remplir la case "nombre de maraudes" de cette ligne
        let maraudesCell = document.createElement('td');
        maraudesCell.textContent = territoire["occurrences"];
        rowBody.appendChild(maraudesCell);  
    });
     
}

